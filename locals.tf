locals {
  name = length(var.name) > 0 ? var.name : random_uuid.this.result

  default_tags = {
    Name = local.name
  }

  tags = merge(var.tags, local.default_tags)
}
