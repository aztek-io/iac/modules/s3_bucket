resource "aws_iam_role" "replication" {
  count = var.replication["enabled"] ? 1 : 0
  name  = substr("${local.name}-replication", 0, 64)

  assume_role_policy = <<-POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": "s3.amazonaws.com"
        },
        "Effect": "Allow"
      }
    ]
  }
  POLICY
}

data "aws_iam_policy_document" "replication" {
  count  = var.replication["enabled"] ? 1 : 0

  statement {
    actions = [
      "s3:GetObjectVersionForReplication",
      "s3:GetObjectVersionAcl",
    ]

    resources = [
      "${aws_s3_bucket.this.arn}/*"
    ]
  }

  statement {
    actions = [
      "s3:ListBucket",
      "s3:GetReplicationConfiguration",
    ]

    resources = [
      aws_s3_bucket.this.arn
    ]
  }

  dynamic "statement" {
    for_each = var.replication["enabled"] ? var.replication["destinations"] : []

    content {
      actions = [
        "s3:ReplicateObject",
        "s3:ReplicateDelete",
        "s3:ObjectOwnerOverrideToBucketOwner",
        "s3:ReplicateTags",
        "s3:GetObjectVersionTagging",
      ]

      resources = [
        "${statement.value}/*"
      ]
    }
  }
}

resource "aws_iam_policy" "replication" {
  count  = var.replication["enabled"] ? 1 : 0
  name   = substr("${local.name}-replication", 0, 64)
  policy = data.aws_iam_policy_document.replication[count.index].json
}

resource "aws_iam_role_policy_attachment" "replication" {
  count      = var.replication["enabled"] ? 1 : 0
  role       = aws_iam_role.replication[0].name
  policy_arn = aws_iam_policy.replication[0].arn
}
