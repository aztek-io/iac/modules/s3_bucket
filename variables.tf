variable "acl" {
  type    = string
  default = "private"
}

variable "logging" {
  type    = bool
  default = true
}

variable "name" {
  type    = string
  default = ""
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "versioning" {
  type    = bool
  default = true
}

variable "replication" {
  type    = object({
    enabled      = bool
    destinations = list(string)
    account_id   = string
  })
  default = {
    enabled      = false
    destinations = []
    account_id   = ""
  }
}

variable "policy" {
  type    = string
  default = ""
}
