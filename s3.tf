resource "aws_s3_bucket" "this" {
  bucket = local.name
  acl    = var.acl
  tags   = local.tags

  versioning {
    enabled = var.versioning
  }

  dynamic "logging" {
    for_each = var.logging ? toset(["this"]) : []
    content {
      target_bucket = aws_s3_bucket.log_bucket[0].id
      target_prefix = "logs/"
    }
  }

  dynamic "replication_configuration" {
    for_each = var.replication["enabled"] ? toset(var.replication["destinations"]) : []

    content {
      role = var.replication["enabled"] ? aws_iam_role.replication[0].arn : ""

      rules {
        id     = replication_configuration.value
        status = var.replication["enabled"] ? "Enabled" : "Disabled"

        destination {
          bucket        = replication_configuration.value
          storage_class = "STANDARD"
          account_id    = var.replication["account_id"]

          access_control_translation {
            owner = "Destination"
          }
        }
      }
    }
  }
}

resource "aws_s3_bucket_policy" "this" {
  count  = length(var.policy) > 0 ? 1 : 0
  bucket = aws_s3_bucket.this.id
  policy = var.policy
}

resource "aws_s3_bucket" "log_bucket" {
  count  = var.logging ? 1 : 0
  bucket = "${local.name}-logs"
  acl    = "log-delivery-write"
}
